<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RequestServiceController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\WorkerController;
use Illuminate\Support\Facades\Route;


// routes for frontend

Route::get('/', [ServiceController::class, 'homepage'])->name('homepage');

Route::get('/request_service/create', [RequestServiceController::class, 'create'])->name('request_services.create');
Route::post('/request_service/store', [RequestServiceController::class, 'store'])->name('request_services.store');
Route::get('/request_service/index', [RequestServiceController::class, 'index'])->name('request_services.index');
Route::get('/request_service/show/{id}', [RequestServiceController::class, 'show'])->name('request_services.show');

Route::get('/service-details/{id}', [ServiceController::class, 'serviceDetails'])->name('service-details');


Route::get('/ac-service', function () {
    return view('frontend.ac-service');
})->name('ac-service');

Route::get('/applirance-repair', function () {
    return view('frontend.applirance-repair');
})->name('applirance-repair');


//routes for backend

Route::get('/admin', function () {
    return view('backend.dashboard');
})->middleware(['auth'])->name('backend.dashboard');

Route::prefix('admin')->middleware(['auth'])->group(function () {
Route::get('/category', [CategoryController::class, 'create'])->name('category.create');
Route::post('/category/store', [CategoryController::class, 'store'])->name('category.store');

Route::get('/category/index', [CategoryController::class, 'index'])->name('category.index');
Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
Route::delete('/category/{category}/destroy', [CategoryController::class, 'destroy'])->name('category.destroy');
Route::post('/category/{category}/update', [CategoryController::class, 'update'])->name('category.update');
Route::get('/deleted-categories', [CategoryController::class, 'trash'])->name('category.trash');


Route::get('/deleted-categories/{id}/restore', [CategoryController::class, 'restore'])->name('category.restore');
Route::delete('/deleted-categories/{id}', [CategoryController::class, 'delete'])->name('category.delete');
    
    
    });


    
    // routes for service
Route::prefix('admin')->group(function () {

Route::get('/service/create', [ServiceController::class, 'create'])->name('service.create');
Route::post('/service/store', [ServiceController::class, 'store'])->name('service.store');
Route::get('/service/index', [ServiceController::class, 'index'])->name('service.index');
Route::get('/service/show/{id}', [ServiceController::class, 'show'])->name('service.show');
Route::get('/service/edit/{service}', [ServiceController::class, 'edit'])->name('service.edit');
Route::get('/service/trash', [ServiceController::class, 'trash'])->name('service.trash');
Route::patch('/service/update/{service}', [ServiceController::class, 'update'])->name('service.update');
Route::delete('/service/destroy/{service}', [ServiceController::class, 'destroy'])->name('service.destroy');
Route::get('/service/restore/{id}', [ServiceController::class, 'restore'])->name('service.restore');
Route::delete('/service/delete/{id}', [ServiceController::class, 'delete'])->name('service.delete');





    });




    Route::prefix('admin')->group(function () {

        Route::get('/worker/create', [WorkerController::class, 'create'])->name('worker.create');
        Route::post('/worker/store', [WorkerController::class, 'store'])->name('worker.store');
        Route::get('/worker/index', [WorkerController::class, 'index'])->name('worker.index');
        Route::get('/worker/show/{id}', [WorkerController::class, 'show'])->name('worker.show');
        
        
        
        
    });








Route::get('/dashboard', function () {
    return view('backend.dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
