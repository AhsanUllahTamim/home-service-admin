

<x-frontend.layouts.master>



  <header class="bg-light  p-3 sticky-top shadow-lg">
    <nav class="navbar navbar-expand-lg container">
        <div class="container-fluid">
          <a class=" navbar-brand fw-bolder text-dark" href="#">Home <small class="fst-italic fw-lighter">Service</small> </a>
            
        <input class="form-control w-25 border border-dark" list="datalistOptions" id="exampleDataList" placeholder="Find your services here...">
            <datalist id="datalistOptions">
                <option value="AC Servicing">
                <option value="Home Cleaning">
                <option value="Painting Service">
                <option value="Daily Mail">
                <option value="Plumbing & Sanitary Services">
            </datalist>
            

            <div class="d-flex fw-bolder">
                <div class="dropdown pe-4">
                    <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                      All Services
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">


                      @foreach ($services as $service)

                      <li><a class="dropdown-item" href="#">{{$service->category}}</a></li>
                        {{-- <option value="{{$key}}">{{$category}}</option> --}}
                        @endforeach
                    
                      <li><a class="dropdown-item" href="#">AC Repair Service</a></li>
                      
                    </ul>
                  </div>
               <a href="{{route('login')}}"> <button class="btn btn-dark" >Login</button></a>
            </div>
        </div>
      </nav>
</header>



<main >
  <div>
    <img src="https://i.ibb.co/wQpm5GP/teams-banner.jpg" class="img-fluid" alt="" srcset="">
  </div>
    <div class="container bg-light bg-opacity-25 shadow-lg p-3 rounded my-3 text-center">
        <div class="row">
          <div class="col">
            <img src="https://i.ibb.co/1ZWjCNk/acicon.jpg" class="rounded mx-auto d-block" height="80px" width="80px"alt="" srcset="">
            <p ><a href="{{route('ac-service')}}">AC Repair Service</a></p>
          </div>
          <div class="col">
            <img src="https://i.ibb.co/Xk866V7/aplianceicon.webp" class="img-fluid rounded mx-auto d-block" height="80px" width="80px"alt="" srcset="">
            <p ><a href="{{route('applirance-repair')}}">Appliarance repair</a></p>
          </div>
          <!-- <div class="col">
            <img src="https://i.ibb.co/PwDBDgC/beauty-Icon.png" class="img-fluid rounded mx-auto d-block" height="80px" width="80px"alt="" srcset="">
            <p >Beauty & Salon</p>
          </div> -->
          <div class="col">
            <img src="https://i.ibb.co/KKrBVyj/tripIcon.jpg" class="img-fluid rounded mx-auto d-block" height="80px" width="80px"alt="" srcset="">
            <p >Trips & Cares</p>
          </div>
          <div class="col">
            <img src="https://i.ibb.co/y4ySxbT/carIcon.png" class="img-fluid rounded mx-auto d-block" height="80px" width="80px"alt="" srcset="">
            <p>Car Care Services</p>
          </div>
          <div class="col">
            <img src="https://i.ibb.co/fXgbxtS/pestconticon.png" class="img-fluid rounded mx-auto d-block" height="80px" width="80px"alt="" srcset="">
            <p >Cleaning & Pest Control</p>
          </div>
        </div>
    </div>



  
    <div class="container pb-5 ">
          <h4 class=" pb-4">For Your Home</h4>
          <div class="container text-center">
              <div class="row">
                

                @foreach($services as $service)
                <div class="col">
                  <div class="card" style="width: 18rem;">
                     <a href="{{route('service-details', [$service->id])}}"> <img src="{{ asset('storage/services/'.$service->image) }}" class="card-img-top" height="186px" width="280px" alt="..."></a>
                      <div class="card-body">
                        <p class="card-text fw-bolder">{{$service->service_title}}</p>
                        <br>
                        <p class="">Rate:{{$service->rate}}tk per hour</p>
                      </div>
                    </div>
                </div>

                @endforeach
                
              </div>
            </div>
      </div>






    <div class="container pb-5">
        <h4 class=" pb-4">Recommended</h4>
        <div class="container text-center">
            <div class="row">
              <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/2W3pfzx/meal.webp" class="card-img-top" height="186px" width="280px"  alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">Daily Meal</p>
                    </div>
                  </div>
              </div>
              <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/g9yMNy5/ac-service.jpg" class="card-img-top" height="186px" width="280px" alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">AC Servicing</p>
                    </div>
                  </div>
              </div>
              <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/vHrNZgP/beauty.jpg" height="186px" width="280px" alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">Personal Beauty care</p>
                    </div>
                  </div>
              </div>
              <!-- <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/ZSHwyBW/home-cleaning.jpg" class="card-img-top" height="186px" width="280px" alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">Home Cleaning</p>
                    </div>
                  </div>
              </div> -->
            </div>
          </div>
    </div>
    <div class="container pb-5">
        <h4 class=" pb-4">Trending</h4>
        <div class="container text-center">
            <div class="row">
              <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/g9yMNy5/ac-service.jpg" class="card-img-top" height="186px" width="280px" alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">AC Servicing</p>
                    </div>
                  </div>
              </div>
              <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/ZSHwyBW/home-cleaning.jpg" class="card-img-top" height="186px" width="280px" alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">Home Cleaning</p>
                    </div>
                  </div>
              </div>
              <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/vHrNZgP/beauty.jpg" class="card-img-top" height="186px" width="280px" alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">Personal Beauty care</p>
                    </div>
                  </div>
              </div>
              <div class="col">
                <div class="card" style="width: 18rem;">
                    <img src="https://i.ibb.co/QC7DN87/pest-control.jpg" class="card-img-top" height="186px" width="280px" alt="...">
                    <div class="card-body">
                      <p class="card-text fw-bolder">Pest Control Service</p>
                    </div>
                  </div>
              </div>
            </div>
          </div>
    </div>
   

</main>

</x-frontend.layouts.master>