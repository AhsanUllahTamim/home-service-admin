<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="{{asset('admin-template/css/sb-admin-2.css')}}">

    <title>SB Admin 2 - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

   

      
                  <div class="container-fluid">



                  <h1 class="mt-4">RequestService</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">RequestService</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            RequestService Create
            
        </div>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{route('request_services.store')}}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="mb-3">
                    <label for="category_id" class="form-label">Category</label>
                    <select name="category" id="category_id" class="form-control">
                        <option value="">Select One</option>
                        @foreach ($categories as $key=>$category)
                        <option value="{{$category}}">{{$category}}</option>
                        @endforeach
                    </select>
                    @error('category')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{ old('name') }}">

                    @error('name')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="phone_num" class="form-label">Phone_number</label>
                    <input name="phone_num" type="tel" class="form-control" id="phone_num" value="{{ old('phone_num') }}">

                    @error('phone_num')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="address" class="form-label">Address</label>
                    <input name="address" type="text" class="form-control" id="address" value="{{ old('address') }}">

                    @error('address')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

               

            

                <div class="mb-3">
                    <label for="rate" class="form-label">Rate per hour</label>
                    <input name="rate" type="text" class="form-control" id="price" value="{{ old('price') }}">
                    @error('price')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea name="description" class="form-control" rows="10" id="description">
                        {{ old('description') }}
                    </textarea>
                    @error('description')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

            

                

                <button type="submit" class="btn btn-primary" onclick="return confirm('Successfully request placed')">Place Request</button>
                <button type="submit" class="btn btn-info"><a href="{{route('homepage')}}">Back to home</a></button>

            </form>
        </div>
    </div>



  
                   
  
                  </div>
                  <!-- /.container-fluid -->
  
              

               
                <!-- /.container-fluid -->

           

           

    <!-- Bootstrap core JavaScript-->


    <script src="{{ asset('admin-template/js/sb-admin-2.js') }}"></script>
    <script src="{{ asset('admin-template/js/sb-admin-2.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('admin-template/js/sb-admin-2.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('admin-template/js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('admin-template/js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('admin-template/js/sb-admin-2.min.js')}}"></script>

</body>

</html>



























