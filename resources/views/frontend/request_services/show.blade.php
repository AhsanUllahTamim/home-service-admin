<x-backend.layouts.master>
    <h1 class="mt-4">Request Services</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Request Services</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Request Services Details
            <a class="btn btn-sm btn-primary" href="{{ route('request_services.index') }}">List</a>
        </div>
        <div class="card-body">
            <h3>Category: {{ $request_services->category ?? 'N/A' }}</h3>
            <h3>Name: {{ $request_services->name }}</h3>
            <p>Phone Number: {{ $request_services->phone_num }}</p>
            <p>Address: {{ $request_services->address }}</p>
            <p>Description: {{ $request_services->description }}</p>
            <p>Rate: {{ $request_services->rate }}</p>
           
        </div>

    </div>
</x-backend.layouts.master>