<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <img src="https://us.123rf.com/450wm/viperagp/viperagp1708/viperagp170800043/83655000-set-of-white-home-appliances-on-a-wooden-floor-3d-illustration.jpg?ver=6" height="450px"; width="100%"/>
    
    <div class="container" style="margin-top: 30px;">
        <div class="row">
          <div class="col-1">
            
          </div>
          <div class="col-11">
            <h1>Microwave Oven Repair Services</h1>
            <div style="height: 30px; width: 180px";>
                <img src="https://cdn3.iconfinder.com/data/icons/sympletts-free-sampler/128/star-512.png" width="25px" height="30px"/>
                <h4 style="margin-left: 25px; float: right;">4.7 out of 5</h4>
            </div>
            <p>(7021 ratings on 3 services)</p>
            <div>
                <ul>
                    <li><h5>24/7 Customer Support</h5></li>
                    <li><h5>Trusted & Reliable Electricians</h5></li>
                    <li><h5>Guaranteed Customer Satisfaction</h5></li>
                </ul>
            </div>
          </div>
        </div>
    </div>

    <br/>

    <div class="container">
        <div class="row">
          <div class="col-3">
            
          </div>
          <div class="col-9">
            <div>
                <h2>
                    <b>Overview of Microwave Oven Repair</b>
                </h2>
                <h5>
                    What's Included
                </h5>
                <div>
                    <ul>
                        <li>Expert Microwave Technicians</li>
                        <li>Replacement of parts and components (If required)</li>
                        <li>Complete repairing work</li>
                        <li>Damage Coverage</li>
                    </ul>
                </div>

                <h5>
                    Service Features
                </h5>
                <div>
                    <ul>
                        <li>Top rated microwave technicians</li>
                        <li>Diagnosis and problem identification</li>
                        <li>Replacement of parts and components (If required)</li>
                        <li>Complete repairing work</li>
                    </ul>
                </div>

                <h5>
                    Available Services
                </h5>
                <div>
                    <ul>
                        <li>Basic Servicing</li>
                        <li>Master Service</li>
                        <li>Water Drop Solution</li>
                        <li>Goods Wash</li>
                    </ul>
                </div>
            </div>

            <div>
                <h2>
                    <b>FAQ</b>
                </h2>
                <div style="margin-left: 30px;">
                    <p><b>Do I have to pay any charge if I don't take any service?</b></p>
                    <p style="margin-left: 30px;">if you don't avail any service for your material after our Service.</p>
                    <p><b>Do I have to pay advance money before availing your service?</b></p>
                    <p><b>Is this only for household product?</b></p>
                    <p><b>What if they damage my microwave?</b></p>
                    <p><b>Do you give Material/Parts warranty?</b></p>
                    <p><b>Can I buy microwave material/parts by myself and ask your technician?</b></p>
                </div>
            </div>

            <div>
                <img src="../images/Microwave.PNG"/>
            </div>

            <div>
                <h2>
                    <b>Details</b>
                </h2>
                <br/>
                <p>Microwave oven plays a very important role in daily life to heat and cook food. When it gets damaged or start dysfunctions, you would require an expert technician to troubleshoot the problem. At Sheba.xyz we offer you a platform to hire experts and professionals to microwave oven repair service. Find the best microwave oven repair service near you through Sheba.xyz and get that faulty microwave oven fixed right away!</p>
            </div>

          </div>
          
        </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    
  </body>
</html>