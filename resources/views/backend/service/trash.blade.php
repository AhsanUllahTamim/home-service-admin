<x-backend.layouts.master>
    <h1 class="mt-4">Services</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Deleted services</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Deleted service List
            <a class="btn btn-sm btn-primary" href="{{ route('service.index') }}">List</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table id="datatablesSimple" class="table table-bordered">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>service Title</th>
                        <th>Category</th>
                        <th>Rate</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($services as $service)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $service->service_title }}</td>
                        <td>{{ $service->category }}</td>
                        <td>{{ $service->rate }}per hour</td>
                        <td>

                            <a class="btn btn-warning btn-sm" href="{{ route('service.restore', ['id' => $service->id]) }}">Restore</a>

                            <form action="{{ route('service.delete', ['id' => $service->id]) }}" method="POST" 
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>