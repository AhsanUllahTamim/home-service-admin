<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Service;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class ServiceController extends Controller
{
    //



    public function create(){



        // $cayegories=Category::orderBy('id')->get();

        $categories = Category::pluck('category_title', 'id')->toArray();

        //  dd($categories);


        return view('backend.service.create',compact('categories'));



    }

    public function store(Request $request){
       

 try{
        $servicedata=$request->all();


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            Image::make($request->file('image'))
                ->resize(300, 200)
                ->save(storage_path() . '/app/public/services/' . $fileName);
                $servicedata['image'] = $fileName;
        }

        // dd($servicedata);

        $service = Service::create($servicedata);

        return Redirect()->route('service.index');

    }

    catch (QueryException $e) 
    
    {
        return redirect()->back()->withInput()->withErrors($e->getMessage());
    }
    }


    public function index()
    {
        // $products = Product::all();
        $services = Service::orderBy('id', 'desc')->get();
        return view('backend.service.index', compact('services'));
    }

    public function show($id){
 
        //  dd($id);

         $service= Service::where('id', $id)->firstOrFail();

        
            // dd($service);


            return view('backend.service.show' , compact('service'));




    }

    public function homepage(){



        $services= Service::orderBy('id', 'desc')->get();

     
        return view('homepage', compact('services'));

    }

    public function serviceDetails($id){

        $service= Service::where('id', $id)->firstOrFail();

         return view('frontend.service.details' , compact('service'));
      
    }

    public function edit(Service $service)
     {

        // dd($service);
    //     $this->authorize('product-edit', $product); 

        $categories = Category::pluck('category_title', 'id')->toArray();
        // $product = Product::findOrFail($id);
        return view('backend.service.edit', compact('service', 'categories'));
    }


    public function update(Request $request ,Service $service)
    {

        try {
            $requestData = $request->all();

            // $product = Product::findOrFail($id);

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                Image::make($request->file('image'))
                    ->resize(300, 200)
                    ->save(storage_path() . '/app/public/products/' . $fileName);
                $requestData['image'] = $fileName;
            } else {
                $requestData['image'] = $service->image;
            }

            $requestData['updated_by'] = auth()->id();
            $service->update($requestData);

            return redirect()->route('service.index')->withMessage('Successfully Updated !');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        // $this->authorize('product-trash-list');

        $services = Service::onlyTrashed()->get();
        return view('backend.service.trash', compact('services'));
    }

    public function destroy(Service $service)
    {

        // $product = Product::findOrFail($id);
        $service->update(['deleted_by'=>auth()->id()]);

        $service->delete();

        return redirect()->route('service.index')->withMessage('Successfully Deleted !');
    }

    public function restore($id){
        Service::withTrashed()
        ->where('id', $id)
        ->restore();
  
        return redirect()->route('service.trash')->withMessage('Successfully Restored !');
      }
    public function delete($id)
    {
        Service::withTrashed()
            ->where('id', $id)
            ->forceDelete();

        return redirect()->route('service.trash')->withMessage('Deleted Successfully');
    }


   
}
